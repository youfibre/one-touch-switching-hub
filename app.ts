import { PostOfficeMessage, EnvelopeAddressee, SwitchMessage } from '@youfibre/otsc';
import express, { Express, Request, Response } from 'express';
import axios from 'axios';

const app: Express = express();
const port = 9000;

app.use(express.json());

axios.get('https://gitlab.com/youfibre/one-touch-switching-directory/-/raw/main/directory.json').then((response) => {
  const directory: {
    rcpIdDirectory: {
      [key: string]: {
        identity: string,
        name: string,
        endpoint: string,
        permittedSources: string[]
      }
    }
  } = response.data;
  console.log('Loaded directory from GitLab.');

  const queue: {
    attempts: number,
    message: SwitchMessage
  }[] = [];

  const last100: SwitchMessage[] = [];

  async function sendFailureWillRetry(message: SwitchMessage) {
    try {
      const source = directory.rcpIdDirectory[message.envelope.source.identity];
      if (!source) {
        console.log('Source not found to sendFailureWillRetry:', message.envelope.source.identity);
        return;
      }

      const failureMessage = {
        envelope: {
          source: postOfficeSource,
          destination: message.envelope.source
        },
        postOfficeMessage: {
          faultCode: '9009',
          faultText: 'The message has not been delivered to the destination but will be retried.'
        }
      } as PostOfficeMessage;

      logMessage(failureMessage);
      await axios.post(source.endpoint, failureMessage);
    } catch {
      console.log('Failed to sendFailureWillRetry:', message.envelope.source.identity);
    }
  }

  async function sendFailure(message: SwitchMessage) {
    try {
      const source = directory.rcpIdDirectory[message.envelope.source.identity];
      if (!source) {
        console.log('Source not found to sendFailure:', message.envelope.source.identity);
        return;
      }

      const failureMessage = {
        envelope: {
          source: postOfficeSource,
          destination: message.envelope.source
        },
        postOfficeMessage: {
          faultCode: '9008',
          faultText: 'Unable to deliver the message to the destination, timed out.'
        },
        test: true
      } as PostOfficeMessage;

      logMessage(failureMessage);
      await axios.post(source.endpoint, failureMessage);
    } catch {
      console.log('Failed to sendFailure:', message.envelope.source.identity);
    }
  }

  function logMessage(message: SwitchMessage) {
    const sourceName = directory.rcpIdDirectory[message.envelope.source.identity]?.name || 'unknown';
    const destinationName = directory.rcpIdDirectory[message.envelope.destination.identity]?.name || 'unknown';
    console.log(
      `[${message.envelope.source.identity} (${sourceName}) -> ${message.envelope.destination.identity} (${destinationName})]`,
      Array.from(Object.keys(message).filter((k) => k !== 'envelope')).join(', ')
    );
  }

  setInterval(async () => {
    if (queue.length === 0) {
      // console.log('No messages to process.');
      return;
    }

    console.log();

    const queueEntry = queue[0];
    queue.splice(0, 1);
    
    try {
      queueEntry.attempts++;
      logMessage(queueEntry.message);

      const destination = directory.rcpIdDirectory[queueEntry.message.envelope.destination.identity];
      await axios.post(
        destination.endpoint,
        queueEntry.message
      );
    } catch (e) {
      console.log(e);

      if (queueEntry.attempts >= 3) {
        return await sendFailure(queueEntry.message);
      }

      queue.push(queueEntry);
      return await sendFailureWillRetry(queueEntry.message);
    }
  }, 2000);

  const postOfficeSource: EnvelopeAddressee = {
    type: 'RCPID',
    identity: directory.rcpIdDirectory.RCP000.identity,
    correlationID: ''
  };

  app.get('/directory/read', (req: Request, res: Response) => {
    res.send(directory);
  });

  app.post('/letterbox/post', (req: Request, res: Response) => {
    const body = req.body;
    if (!body) {
      console.log('no req body');
      return res.status(400).send('Bad request.');
    }

    // @TODO: Add authentication

    queue.push({
      attempts: 0,
      message: body
    });

    last100.push(body);
    if (last100.length > 100) {
      last100.splice(0, 1);
    }

    return res.send({
      status: 'acknowledged'
    });
  });

  app.get('/letterbox/history', (req: Request, res: Response) => {
    res.send({
      last100
    });
  });

  app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
  });
});